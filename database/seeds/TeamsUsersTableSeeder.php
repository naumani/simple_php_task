<?php

use Illuminate\Database\Seeder;

class TeamsUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teamsUsers = [
            ['id' => 1, 'team_id' => 1,'user_id'=>1],
            ['id' => 2, 'team_id' => 2,'user_id'=>1],
            ['id' => 3, 'team_id' => 3,'user_id'=>2],
            ['id' => 4, 'team_id' => 1,'user_id'=>3],
        ];
        DB::table('teams_users')->insert($teamsUsers);
    }
}
