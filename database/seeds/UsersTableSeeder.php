<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            ['id' => 1, 'first_name' => 'Scott', 'last_name' => 'Sommerfeldt'],
            ['id' => 2, 'first_name' => 'Kevin', 'last_name' => 'Cris'],
            ['id' => 3, 'first_name' => 'Tom', 'last_name' => 'Cruise']
        ];
        DB::table('users')->insert($users);
    }
}
