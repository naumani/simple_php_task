<?php

use Illuminate\Database\Seeder;

class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teams = [
            ['id' => 1, 'name' => 'Team 1'],
            ['id' => 2, 'name' => 'Team 2'],
            ['id' => 3, 'name' => 'Team 3']
        ];
        DB::table('teams')->insert($teams);
    }
}
