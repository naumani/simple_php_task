<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{url('/images/' . domainFavIcon())}}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Simple App') }}</title>

    <!-- Styles -->
{!! include_fonts() !!}
{!! include_common_css() !!}
{!! include_common_js() !!}
@yield('css')
@yield('js')
<!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
<div class="wrapper">
    <div class="cotainer">
        {!!  site_header() !!}
        <div class="main-content-section">
            {{ alert_placeholder('','alert_placeholder') }}
            <div class="col-sm-9 {{((Auth::user())?'col-md-10':'col-md-12')}} ">
                <div class="content-col">
                    @yield('content')
                </div>
            </div>
            <div class="clearfix"></div>
            <div id="confirm_popup_modals_container"></div>
            <footer>&copy; {{ date("Y") }} Simple Technologies, LLC. All Rights Reserved.</footer>
        </div>
    </div>
</div>
@yield('extra_js')
</body>
</html>