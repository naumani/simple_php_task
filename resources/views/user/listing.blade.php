@extends('layouts.default')
@section('content')
    <div class="heading-col">
        <h2><i class="fa fa-user"></i>Users</h2>
    </div>
    <!----------------------------------------top search-criteria------------------->
@php  $containerId = 'user_list'; @endphp
    <div class="row">
        <div class="col-md-12">
            <div id="{{ $containerId }}">
                @php
                    $dt_config = dt_default_config();
                    $dt_config['order_col_indx'] = '0';
                    $dt_config['order'] = 'asc';
                    echo dataTable_script('user_TBL', $dt_config);
                @endphp
                <table class="table table-bordered" id="user_TBL">
                    <thead>
                    <tr class="green-bar">
                        <th class="center">Id</th>
                        <th class="center">Name</th>
                        <th class="center">Team</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($users)&&!empty($users))
                        @foreach($users as $user)
                            <tr>
                                <td class="center">{{ isset($user['id'])?$user['id']:0}}</td>
                                <td class="center">{{ isset($user['first_name']) && isset($user['last_name'])? ucfirst($user['first_name']).' '.ucfirst($user['last_name']):'--'}}</td>
                                <td class="center">
                                   {{ isset($user['user_teams'])?$user['user_teams']:'--' }}
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection