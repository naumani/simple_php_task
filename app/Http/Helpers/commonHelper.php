<?php
if (!function_exists('is_logged_in')) {
    function is_logged_in()
    {
        return (loggedinId() > 0) ? true : false;
    }
}

if (!function_exists('loggedinId')) {
    function loggedinId()
    {
        return session('user_id', '0');
    }
}