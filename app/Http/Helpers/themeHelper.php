<?php

use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

if (!function_exists('base_url')) {
    function base_url($extra_url = '')
    {
        return url("/$extra_url");
    }
}
if (!function_exists('domain_url')) {
    function domain_url()
    {
        return "www.sample.com";
    }
}
if (!function_exists('public_site_url')) {
    function public_site_url($extra_url = '')
    {
        return url("/$extra_url");
    }
}
if (!function_exists('domainLogo')) {
    function domainLogo($logo = 'logo.png')
    {
        return $logo;
    }
}
if (!function_exists('domainFavIcon')) {
    function domainFavIcon($logo = 'logo.png')
    {
        return $logo;
    }
}

if (!function_exists('include_common_css')) {
    function include_common_css()
    {
        $type = 'text/css';
        $rel = 'stylesheet';
        $css = '<link rel="' . $rel . '" type="' . $type . '" href="' . asset("css/style.css") . '" />';
        $css .= '<link rel="' . $rel . '" type="' . $type . '" href="' . asset("css/data_table_css/jquery.dataTables.min.css") . '" />';
        $css .= '<link rel="' . $rel . '" type="' . $type . '" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />';
        $css .= '<link rel="' . $rel . '" type="' . $type . '" href="' . asset("css/jquery-ui.css") . '" />';
        echo $css;
    }
}

if (!function_exists('include_css')) {
    function include_css($href)
    {
        $type = 'text/css';
        $rel = 'stylesheet';
        $css = '<link rel="' . $rel . '" type="' . $type . '" href="' . asset($href) . '" />';
        echo $css;
    }
}

if (!function_exists('include_fonts')) {
    function include_fonts()
    {
        $type = 'text/css';
        $rel = 'stylesheet';
        $fonts = "<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='$rel' type='$type'>";
        echo $fonts;
    }
}

if (!function_exists('include_common_js')) {
    function include_common_js()
    {
        $js = '<script>var baseURL = ' . json_encode(url('/')) . '</script>';
        $js .= '<script src="//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js" type="application/javascript"></script>';
        $js .= '<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.0/jquery-confirm.min.js" type="application/javascript"></script>';
        $js .= '<script src="' . asset("js/bootstrap.js") . '" type="application/javascript"></script>';
        $js .= '<script src="' . asset("js/data_table_js/jquery.dataTables.js") . '" type="application/javascript"></script>';
        $js .= '<script src="' . asset('js/jquery-ui.js') . '" type="application/javascript"></script>';
        echo $js;
    }
}
if (!function_exists('site_header')) {
    function site_header()
    {
        $html = '<header>';
        $html .= '<div class="logo"><a href="' . ((is_logged_in()) ? base_url() : public_site_url()) . '"><img src="' . url('/images/' . domainLogo()) . '" height="99px"  /></a></div>';
        $html .= '<div class="user-info-row">';
        $html .= '<ul>';
        $html .= '</ul>';
        $html .= '</div>';
        $html .= '<div class="clearfix"></div>';
        $html .= '</header>';

        echo $html;
    }
}


/*
* For JS Alert messages a placeholder
*/
if (!function_exists('alert_placeholder')) {
    function alert_placeholder($flag = false, $id = 'alert_placeholder')
    {
        if ($flag)
            return '<div id="' . $id . '"></div>';
        else
            echo '<div id="' . $id . '"></div>';
    }
}

/*
* For PHP alert messages
*/
if (!function_exists('alert_message')) {
    function alert_message($message = '', $class = 'alert-warning', $id = 'alert_msg_div')
    {
        $html = '<div class="alert ' . $class . ' alert-dismissible fade in" role="alert" id="' . $id . '">';
        $onClick = "addRemoveClsById('" . $id . "','hidden')";
        $html .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="' . $onClick . '">';
        $html .= '<span aria-hidden="true">&times;</span>';
        $html .= '</button>';
        $html .= '<strong>' . $message . '</strong>';
        $html .= '</div>';
        echo $html;
    }
}
if (!function_exists('toolTip_script')) {
    function toolTip_script()
    {
        $tooltip = 'tooltip';
        $tooltip = "'" . $tooltip . "'";
        $script = '<script>$(function () {
              $("[data-toggle=' . $tooltip . ']").tooltip();';
        $script .= '$("a").tooltip();';
        $script .= '        }
        );</script>';
        return $script;
    }
}
//fix Scroll and focus issue with multiple modals
if (!function_exists('moodal_check_exist')) {
    function moodal_check_exist()
    {
        $script = "<script>$('.modal').on('hidden.bs.modal', function (e) {
            if($('.modal').hasClass('in')) {
            $('body').addClass('modal-open');
            }    
    });</script>";
        return $script;
    }
}
if (!function_exists('dt_default_config')) {

    function dt_default_config()
    {
        $config['b_sorting'] = true; // Sorting
        $config['order_col_indx'] = 0; // column index on which default sort would apply
        $config['order'] = 'desc';
        $config['paging_type'] = 'full'; // template of paging
        $config['paging_length'] = 10; // number of rows per page
        $config['b_info'] = 'true'; //Enable or disable the table information display
        $config['empty_table'] = 'No data available in table';
        $config['paging_first'] = "<i class='fa fa-angle-double-left'></i> First"; // First button text
        $config['paging_last'] = "Last <i class='fa fa-angle-double-right'></i>"; // Last button text
        $config['paging_next'] = "<i class='fa fa-angle-right'></i>"; // Next button text
        $config['paging_previous'] = "<i class='fa fa-angle-left'></i>"; // Previous button text
        $config['drill_down'] = false;
        $config['drill_field'] = '';
        $config['drill_action'] = '';
        $config['drill_route'] = '';
        $config['length_change'] = 'false'; // show pagination lenght menu
        return $config;
    }
}
if (!function_exists('dataTable_script')) {
    function dataTable_script($containerId, $config = array())
    {
        $config = ($config) ? $config : dt_default_config();
        $script = '<script>$(function () { 
              var ' . $containerId . ' = $("#' . $containerId . '").DataTable({
					"bSort": "' . $config['b_sorting'] . '",
					"order": [[ ' . $config['order_col_indx'] . ', "' . $config['order'] . '" ]],
                    "columnDefs": [{ targets: "no-sort", orderable: false}],
					"pagingType": "' . $config['paging_type'] . '",
					"pageLength": ' . $config['paging_length'] . ',
                    "bFilter": false,
                    "bLengthChange": ' . $config['length_change'] . ',
                    "aLengthMenu": [[10, 25, 50, 75, 100, -1], [10, 25, 50, 75, 100, "All"]],
					"bInfo": ' . $config['b_info'] . ',
					"language": {
				           "info": "_START_ - _END_ of _TOTAL_",
						   "infoEmpty": "0 - 0 of 0",
						   "emptyTable": "' . $config['empty_table'] . '",
						   "paginate": {
				             "first": "' . $config['paging_first'] . '",
							 "last": "' . $config['paging_last'] . '",
							 "next": "' . $config['paging_next'] . '",
							 "previous": "' . $config['paging_previous'] . '",
				           }
				         },';

        $script .= '});';
        $script .= dt_drill_listener($containerId, $config);
        $script .= '}
        );</script>';
        return $script;
    }
}


/*## Ticket #2176*/
if (!function_exists('dt_drill_listener')) {
    function dt_drill_listener($containerId, $config = array())
    {
        $script = '';
        if (isset($config['drill_down']) && $config['drill_down']) {
            $config = ($config) ? $config : dt_default_config();
            $script = '// Add event listener for opening and closing details
				$("#' . $containerId . ' tbody").on("click", "td.details-control", function () {
					var tr = $(this).closest("tr");										
					var field_value=$(this).attr("value");					
					var row = ' . $containerId . '.row( tr );			 
					if ( row.child.isShown() ) {					
						// This row is already open - close it
						row.child.hide();
						tr.removeClass("shown");
					}
					else {					
						// Open this row
						  var config = ' . json_encode($config) . ';
					    drilldownReport(row,field_value,config);
						tr.addClass("shown");
					}
				} );';
        }
        return $script;
    }
}

if (!function_exists('include_js')) {
    function include_js($js_name, $flag = false)
    {
        $js = '<script src="' . asset($js_name) . '"></script>';
        if ($flag)
            return $js;
        else
            echo $js;
    }
}