<?php

if (!function_exists('debug')) {
    function debug($data, $is_exit = false)
    {
        if (is_array($data) || is_object($data)) {
            echo '<pre>';
            print_r($data);
            echo '</pre>';
        } else
            echo "$data<br>";

        if ($is_exit)
            exit();
    }

}

if (!function_exists('include_helper')) {
    function include_helper($helperName = '')
    {
        $helper_path = app_path() . '/Http/Helpers/' . $helperName . 'Helper.php';
        if (file_exists($helper_path)) {
            require_once $helper_path;
        }
    }
}

if (!function_exists('model_path')) {
    function model_path($name)
    {
        $str = "App\\Http\\Models\\" . $name;
        return $str;
    }
}