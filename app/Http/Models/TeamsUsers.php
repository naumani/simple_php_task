<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class TeamsUsers extends Model
{
    protected $table = 'teams_users';

    public $fillable = ['id', 'team_id', 'user_id'];

    public function setUpdatedAt($value)
    {
        // Do nothing.
    }

    public function setCreatedAt($value)
    {
        // Do nothing.
    }

    public function team()
    {
        return $this->belongsTo(model_path('Team'), 'team_id');
    }

    public function user()
    {
        return $this->belongsTo(model_path('User'));
    }
}
