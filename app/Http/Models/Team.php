<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = 'teams';

    public $fillable = ['id', 'name', 'created_at'];

    public function setUpdatedAt($value)
    {
        // Do nothing.
    }


    public function teamUsers()
    {
        return $this->hasMany(model_path('TeamsUsers'), 'team_id');
    }
}