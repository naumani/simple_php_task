<?php

namespace App\Http\Controllers;

use App\Http\Models\User;
use App\Http\Models\Team;
use App\Http\Models\TeamsUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Storage;

class UserController extends Controller
{

    public function __construct()
    {

    }

    public function index()
    {

        $usersData = User::with('userTeams.team')->get();
        $users = $this->prepareUserStats($usersData);
        return view('user.listing', compact('users'));
    }

    public function prepareUserStats($usersData=[])
    {
        $users = [];
        if (isset($usersData) && !empty($usersData)) {
            $usersData = $usersData->toArray();
            foreach ($usersData as $userData) {
                $users[$userData['id']]['id'] = isset($userData['id']) ? intval($userData['id']) : 0;
                $users[$userData['id']]['first_name'] = isset($userData['first_name']) ? trim($userData['first_name']) : '';
                $users[$userData['id']]['last_name'] = isset($userData['last_name']) ? trim($userData['last_name']) : '';
                if (isset($userData['user_teams']) && !empty($userData['user_teams'])) {
                    $userTeam = [];
                    foreach ($userData['user_teams'] as $userTeamData) {
                        if (isset($userTeamData['team']['name']) && trim($userTeamData['team']['name']) != '')
                            $userTeam[] = trim($userTeamData['team']['name']);
                    }
                    $users[$userData['id']]['user_teams'] = implode(',', $userTeam);
                    ## I have used here, custom logic to add Team names, we can also use MySQL Case Function but that was not a good option because that adds load on the Database resources, thanks
                }
            }
        }
        return $users;
    }

    public function testMethod()
    {
        $users = User::with('userTeams.team')->get();
        debug($users->toArray(), true);
    }

}